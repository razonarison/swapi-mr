'use strict'
 
const Hapi = require('@hapi/hapi');
 
const server = Hapi.server({
    host: 'localhost',
    port: 3001
});
 
server.route({
    method: 'POST',
    path: '/login',
    handler: function(request, h) {
        return h.response({token: "test123"});
    }
});
 
const checkOrigin = (origin) => {
    if(origin === 'http://localhost:3000'){
        return true
    }else{
        return false
    }
}
 
const start = async function() {
    try {
        await server.register({
            plugin: require('hapi-cors'),
            options: {
                checkOrigin: checkOrigin
            }
        })
 
        await server.start();
    } catch(err) {
        console.log(err);
        process.exit(1);
    }
};
 
start();