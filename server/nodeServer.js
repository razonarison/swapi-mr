"use strict";

const http = require("http");
const express = require("express");
const cors = require("cors");
const constant = require("../util/constant");
const axios = require("axios");
const bodyParser = require("body-parser");

const app = express();
const nodePort = 8081;

app.use(cors());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

// PLANET
app.get("/people/", (req, res, next) => {
  axios
    .get(constant.BASE_URL + "people/")
    .then(response => {
      console.log(response);
      res.send(response.data);
    })
    .catch(error => {
      next(error);
    });
});

app.get("/people/:id", (req, res, next) => {
  let id = req.params.id;
  axios
    .get(constant.BASE_URL + `people/${id}/`)
    .then(response => {
      console.log(response);
      res.send(response.data);
    })
    .catch(error => {
      next(error);
    });
});

app.get("/planets/", (req, res, next) => {
  axios
    .get(constant.BASE_URL + "planets/")
    .then(response => {
      console.log(response);
      res.send(response.data);
    })
    .catch(error => {
      next(error);
    });
});

app.get("/planets/:id", (req, res, next) => {
  let id = req.params.id;
  axios
    .get(constant.BASE_URL + `planets/${id}/`)
    .then(response => {
      console.log(response);
      res.send(response.data);
    })
    .catch(error => {
      next(error);
    });
});

app.get("/starships/", (req, res, next) => {
  axios
    .get(constant.BASE_URL + "starships/")
    .then(response => {
      console.log(response);
      res.send(response.data);
    })
    .catch(error => {
      next(error);
    });
});

app.get("/starships/:id", (req, res, next) => {
  let id = req.params.id;
  axios
    .get(constant.BASE_URL + `starships/${id}/`)
    .then(response => {
      console.log(response);
      res.send(response.data);
    })
    .catch(error => {
      next(error);
    });
});

app.listen(nodePort, () => {
  console.log(`Listen on port ${nodePort}`);
});
