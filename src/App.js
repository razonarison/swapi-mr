import "./App.css";

import Navigation from "./squelette/Navigation";

import "bootstrap/dist/css/bootstrap.min.css";
import "./assets/css/styles.css";

function App() {
  return (
    <div>
      <Navigation />
    </div>
  );
}

export default App;
