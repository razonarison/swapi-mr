import axios from "axios";

const DatasDAO = (url) => {
    return axios(url)
      .then(response => response.data)
      .catch(error => {
        throw error;
      });
  };

export default DatasDAO;