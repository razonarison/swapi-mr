import React, { useState } from "react";
import PropTypes from "prop-types";

import "bootstrap/dist/css/bootstrap.min.css";

async function loginUser(credentials) {
  return fetch("http://localhost:3001/login", {
    method: "POST",
    headers: {
      "Content-Type": "application/json"
    },
    body: JSON.stringify(credentials)
  }).then(data => data.json());
}

export default function Login({ setToken }) {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [messageError, setMessageError] = useState("");

  function inputNameUpdate(event) {
    setUsername(event.target.value);
  }

  function inputPasswdUpdate(event) {
    setPassword(event.target.value);
  }

  async function handleClick(e) {
    e.preventDefault();
    if (username.trim() === "Luke" && password.trim() === "DadSucks") {
      const token = await loginUser({
        username,
        password
      });
      setToken(token);
      console.log("---- token ", token);
    } else {
      setMessageError("Indentifiants invalides");
    }
  }

  return (
    <div className="bg-primary">
      <div id="layoutAuthentication">
        <div id="layoutAuthentication_content">
          <main>
            <div className="container">
              <div className="row justify-content-center">
                <div className="col-lg-5">
                  <div className="card shadow-lg border-0 rounded-lg mt-5">
                    <div className="card-header">
                      <h3 className="text-center font-weight-light my-4">
                        Login
                      </h3>
                    </div>
                    <div className="card-body">
                      <form>
                        <div className="form-group">
                          <label className="small mb-1" htmlFor="UserName">
                            UserName
                          </label>
                          <input
                            className="form-control py-4"
                            id="inputEmailAddress"
                            type="text"
                            placeholder="Enter email address"
                            onChange={inputNameUpdate}
                          />
                        </div>
                        <div className="form-group">
                          <label className="small mb-1" htmlFor="inputPassword">
                            Password
                          </label>
                          <input
                            className="form-control py-4"
                            id="inputPassword"
                            type="password"
                            placeholder="Enter password"
                            onChange={inputPasswdUpdate}
                          />
                        </div>
                        <div className="form-group d-flex align-items-center justify-content-between mt-4 mb-0">
                          <span>{messageError}</span>

                          <button
                            className="btn btn-primary"
                            onClick={handleClick}
                          >
                            Login
                          </button>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </main>
        </div>
      </div>
    </div>
  );
}

Login.propTypes = {
  setToken: PropTypes.func.isRequired
};
