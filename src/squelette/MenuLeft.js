import React from "react";
import { Link } from "react-router-dom";
function MenuLeft() {
  return (
    <div id="layoutSidenav_nav">
      <nav
        className="sb-sidenav accordion sb-sidenav-dark"
        id="sidenavAccordion"
      >
        <div className="sb-sidenav-menu">
          <div className="nav">
            <Link className="nav-link" to="/people">
              <div className="sb-nav-link-icon">
                <i className="fas fa-tachometer-alt"></i>
              </div>
              People
            </Link>
            <Link className="nav-link" to="/planet">
              <div className="sb-nav-link-icon">
                <i className="fas fa-tachometer-alt"></i>
              </div>
              Planets
            </Link>
            <a className="nav-link" href="/starship">
              <div className="sb-nav-link-icon">
                <i className="fas fa-tachometer-alt"></i>
              </div>
              Starships
            </a>
          </div>
        </div>
      </nav>
    </div>
  );
}

export default MenuLeft;
