import React from "react";
import { Redirect } from "react-router";
import { BrowserRouter as Router , Switch, Route } from "react-router-dom";
import PeopleScreen from "./PeopleScreen";
import PlanetScreen from "./PlanetScreen";
import StarshipScreen from "./StarshipScreen";
import FichePeople from "./fiche/FichePeople";
import FichePlanet from "./fiche/FichePlanet";
import FicheStarship from "./fiche/FicheStarship";
import Accueil from "./Accueil";

function Navigation() {
  return (
    <Router>
      <Switch>
        <Redirect exact from="/" to="/accueil" />
        <Route exact path="/accueil">
          <Accueil />
        </Route>
        <Route exact path="/people">
          <PeopleScreen />
        </Route>
        <Route exact path="/planet">
          <PlanetScreen />
        </Route>
        <Route exact path="/starship">
          <StarshipScreen />
        </Route>      
        <Route path="/fiche-people/:id" component={FichePeople}/>
        <Route path="/fiche-planet/:id" component={FichePlanet}/>
        <Route path="/fiche-starship/:id" component={FicheStarship}/>
      </Switch>
    </Router>
  );
}

export default Navigation;
