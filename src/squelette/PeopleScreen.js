import React, { useState, useEffect } from "react";
import "../assets/css/mine.css";
import dateFormat from "dateformat";
import DatasDAO from "../dao/DatasDAO";
import MenuLeft from "./MenuLeft";
import { Link } from "react-router-dom";

function PlanetScreen() {
  const [datas, setDatas] = useState([]);
  const [datasSearchCriteria, setDatasSearchCriteria] = useState([]);
  const [name, setName] = useState("");
  const [gender, setGender] = useState("");
  const [massMin, setMassMin] = useState("");
  const [massMax, setMassMax] = useState("");

  const urlPeopleDatas = "http://localhost:8081/people/";

  useEffect(() => {
    DatasDAO(urlPeopleDatas).then(res => {
      setDatas(res.results);
      setDatasSearchCriteria(res.results);
    });
  }, []);

  function inputNameUpdated(event) {
    setName(event.target.value);
  }
  function inputGenderUpdated(event) {
    setGender(event.target.value);
  }
  function inputMassMinUpdated(event) {
    setMassMin(event.target.value);
  }
  function inputMassMaxUpdated(event) {
    setMassMax(event.target.value);
  }

  function handleSearch(event) {
    const datasTemp = datasSearchCriteria.slice();
    const datasSearch = datasTemp.filter(el => {
      const nameCondition = name ? el.name.includes(name) : true;
      const genderCondition = gender ? el.gender.includes(gender) : true;
      const massMinCondition = massMin
        ? parseInt(el.mass) >= parseInt(massMin)
        : true;
      const massMaxCondition = massMax
        ? parseInt(el.mass) <= parseInt(massMax)
        : true;
      return (
        nameCondition && genderCondition && massMinCondition && massMaxCondition
      );
    });
    setDatas(datasSearch);
  }

  function undoSearch() {
    DatasDAO(urlPeopleDatas).then(res => {
      setDatas(res.results);
    });
  }

  return (
    <div className="sb-nav-fixed">
      <nav className="sb-topnav navbar navbar-expand navbar-dark bg-dark">
        <a className="navbar-brand" href="index.html">
          SWAPI
        </a>
      </nav>
      <div id="layoutSidenav">
        <MenuLeft />
        <div>
          <div id="layoutSidenav_content">
            <main>
              <div className="container-fluid">
                <h1 className="mt-4">People</h1>
                <div className="row form-container">
                  <div className="col-xl-3 col-md-6">
                    <div className="form-group">
                      <label htmlFor="name">Name</label>
                      <input
                        className="form-control"
                        name="name"
                        type="text"
                        onChange={inputNameUpdated}
                      />
                    </div>
                  </div>
                  <div className="col-xl-3 col-md-6">
                    <div className="form-group">
                      <label htmlFor="gender">Gender</label>
                      <input
                        className="form-control"
                        name="gender"
                        type="text"
                        onChange={inputGenderUpdated}
                      />
                    </div>
                  </div>
                  <div className="col-xl-3 col-md-6">
                    <div className="form-group">
                      <label htmlFor="massMin">Mass min</label>
                      <input
                        className="form-control"
                        name="massMin"
                        type="text"
                        onChange={inputMassMinUpdated}
                      />
                    </div>
                  </div>
                  <div className="col-xl-3 col-md-6">
                    <div className="form-group">
                      <label htmlFor="massMax">Mass max</label>
                      <input
                        className="form-control"
                        name="massMax"
                        type="number"
                        onChange={inputMassMaxUpdated}
                      />
                    </div>
                  </div>
                  <div className="form-group">
                    <button
                      className="btn btn-md btn-primary btn-search"
                      onClick={handleSearch}
                    >
                      Rechercher
                    </button>
                    <button
                      className="btn btn-primary btn-success"
                      onClick={undoSearch}
                    >
                      Réinitialiser
                    </button>
                  </div>
                </div>

                <table className="table">
                  <thead>
                    <tr>
                      <th scope="col">#</th>
                      <th scope="col">Name</th>
                      <th scope="col">Height</th>
                      <th scope="col">Created on</th>
                      <th scope="col">Edited on</th>
                    </tr>
                  </thead>

                  {datas &&
                    datas.map((data, i) => {
                      return (
                        <tbody key={i}>
                          <tr>
                            <th scope="row">
                              <Link
                                to={{
                                  pathname: `/fiche-people/${i + 1}`,
                                  state: {
                                    foo: { i }
                                  }
                                }}
                              >
                                {i + 1}
                              </Link>
                            </th>
                            <td>{data.name}</td>
                            <td>{data.height}</td>
                            <td>{dateFormat(data.created, "dd/mm/yy")}</td>
                            <td>{dateFormat(data.editedm, "dd/mm/yy")}</td>
                          </tr>
                        </tbody>
                      );
                    })}
                </table>
              </div>
            </main>
          </div>
        </div>
      </div>
    </div>
  );
}

export default PlanetScreen;
