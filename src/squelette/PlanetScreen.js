import React, { useState, useEffect } from "react";
import "../assets/css/mine.css";
import dateFormat from "dateformat";
import DatasDAO from "../dao/DatasDAO";
import { Link } from "react-router-dom";
import MenuLeft from "./MenuLeft";

function PlanetScreen() {
  const [datas, setDatas] = useState([]);
  const [datasSearchCriteria, setDatasSearchCriteria] = useState([]);
  const [name, setName] = useState("");
  const [rotationMin, setRotationMin] = useState("");
  const [rotationMax, setRotationMax] = useState("");
  const [populationMin, setPopulationMin] = useState("");
  const [populationMax, setPopulationMax] = useState("");

  const urlPlanetsDatas = "http://localhost:8081/planets/";

  useEffect(() => {
    DatasDAO(urlPlanetsDatas).then(res => {
      setDatas(res.results);
      setDatasSearchCriteria(res.results);
    });
  }, []);

  function inputNameUpdated(event) {
    setName(event.target.value);
  }
  function inputRotationMinUpdated(event) {
    setRotationMin(event.target.value);
  }
  function inputRotationMaxUpdated(event) {
    setRotationMax(event.target.value);
  }
  function inputPopulationMinUpdated(event) {
    setPopulationMin(event.target.value);
  }
  function inputPopulationMaxUpdated(event) {
    setPopulationMax(event.target.value);
  }

  function handleSearch(event) {
    const datasTemp = datasSearchCriteria.slice();
    const datasSearch = datasTemp.filter(el => {
      const nameCondition = name ? el.name.includes(name) : true;
      const rotationMinCondition = rotationMin
        ? parseInt(el.rotation_period) >= parseInt(rotationMin)
        : true;
      const rotationMaxCondition = rotationMax
        ? parseInt(el.rotation_period) <= parseInt(rotationMax)
        : true;
      const populationMinCondition = populationMin
        ? parseInt(el.population) >= parseInt(populationMin)
        : true;
      const populationMaxCondition = populationMax
        ? parseInt(el.population) <= parseInt(populationMax)
        : true;
      return (
        nameCondition &&
        rotationMinCondition &&
        rotationMaxCondition &&
        populationMinCondition &&
        populationMaxCondition
      );
    });
    setDatas(datasSearch);
  }

  function undoSearch() {
    DatasDAO(urlPlanetsDatas).then(res => {
      setDatas(res.results);
    });
  }

  return (
    <div className="sb-nav-fixed">
      <nav className="sb-topnav navbar navbar-expand navbar-dark bg-dark">
        <a className="navbar-brand" href="index.html">
          SWAPI
        </a>
      </nav>
      <div id="layoutSidenav">
        <MenuLeft />
        <div id="layoutSidenav_content">
          <main>
            <div className="container-fluid">
              <h1 className="mt-4">Planet</h1>
              <div className="row form-container">
                <div className="col-xl-3 col-md-12">
                  <div className="form-group">
                    <label htmlFor="name">Name</label>
                    <input
                      className="form-control"
                      name="name"
                      type="text"
                      onChange={inputNameUpdated}
                    />
                  </div>
                </div>
                <div className="col-xl-3 col-md-6">
                  <div className="form-group">
                    <label htmlFor="massMin">Rotation min</label>
                    <input
                      className="form-control"
                      name="massMin"
                      type="text"
                      onChange={inputRotationMinUpdated}
                    />
                  </div>
                </div>
                <div className="col-xl-3 col-md-6">
                  <div className="form-group">
                    <label htmlFor="massMax">Rotation max</label>
                    <input
                      className="form-control"
                      name="massMax"
                      type="number"
                      onChange={inputRotationMaxUpdated}
                    />
                  </div>
                </div>
                <div className="col-xl-3 col-md-6">
                  <div className="form-group">
                    <label htmlFor="massMax">Population min</label>
                    <input
                      className="form-control"
                      name="massMax"
                      type="number"
                      onChange={inputPopulationMinUpdated}
                    />
                  </div>
                </div>
                <div className="col-xl-3 col-md-6">
                  <div className="form-group">
                    <label htmlFor="massMax">Population max</label>
                    <input
                      className="form-control"
                      name="massMax"
                      type="number"
                      onChange={inputPopulationMaxUpdated}
                    />
                  </div>
                </div>
                <div className="col-md-12">
                  <button
                    className="btn btn-primary btn-search"
                    onClick={handleSearch}
                  >
                    Rechercher
                  </button>
                  <button
                    className="btn btn-primary btn-success"
                    onClick={undoSearch}
                  >
                    Réinitialiser
                  </button>
                </div>
              </div>
              <table className="table">
                <thead>
                  <tr>
                    <th scope="col">#</th>
                    <th scope="col">Name</th>
                    <th scope="col">Climate</th>
                    <th scope="col">Created on</th>
                    <th scope="col">Edited on</th>
                  </tr>
                </thead>

                {datas.map((data, i) => {
                  return (
                    <tbody key={i}>
                      <tr>
                        <th scope="row">
                          <Link
                            to={{
                              pathname: `/fiche-planet/${i + 1}`,
                              state: {
                                foo: { i }
                              }
                            }}
                          >
                            {i + 1}
                          </Link>
                        </th>
                        <td>{data.name}</td>
                        <td>{data.climate}</td>
                        <td>{dateFormat(data.created, "dd/mm/yy")}</td>
                        <td>{dateFormat(data.editedm, "dd/mm/yy")}</td>
                      </tr>
                    </tbody>
                  );
                })}
              </table>
            </div>
          </main>
        </div>
      </div>
    </div>
  );
}

export default PlanetScreen;
