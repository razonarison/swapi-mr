import React, { useState, useEffect } from "react";
import dateFormat from "dateformat";
import DatasDAO from "../dao/DatasDAO";
import { Link } from "react-router-dom";
import MenuLeft from "./MenuLeft";

function StarshipScreen() {
  const [datas, setDatas] = useState([]);
  const [datasSearchCriteria, setDatasSearchCriteria] = useState([]);
  const [name, setName] = useState("");
  const [model, setModel] = useState("");
  const [manufacturer, setManufacturer] = useState("");

  const urlStarshipsDatas = "http://localhost:8081/starships/";

  function inputNameUpdated(event) {
    setName(event.target.value);
  }
  function inputModelUpdated(event) {
    setModel(event.target.value);
  }
  function inputManufacturerUpdated(event) {
    setManufacturer(event.target.value);
  }

  useEffect(() => {
    DatasDAO(urlStarshipsDatas).then(res => {
      setDatas(res.results);
      setDatasSearchCriteria(res.results);
    });
  }, []);

  function handleSearch(event) {
    const datasTemp = datasSearchCriteria.slice();
    const datasSearch = datasTemp.filter(el => {
      const nameCondition = name ? el.name.includes(name) : true;
      const modelCondition = model ? el.model.includes(model) : true;
      const manufacturerCondition = manufacturer
        ? el.manufacturer.includes(manufacturer)
        : true;
      return nameCondition && modelCondition && manufacturerCondition;
    });
    setDatas(datasSearch);
  }

  function undoSearch() {
    DatasDAO(urlStarshipsDatas).then(res => {
      setDatas(res.results);
    });
  }

  return (
    <div className="sb-nav-fixed">
      <nav className="sb-topnav navbar navbar-expand navbar-dark bg-dark">
        <a className="navbar-brand" href="index.html">
          SWAPI
        </a>
      </nav>
      <div id="layoutSidenav">
        <MenuLeft />
        <div id="layoutSidenav_content">
          <main>
            <div className="container-fluid">
              <h1 className="mt-4">Starship</h1>
              <div className="row form-container">
                <div className="col-xl-3 col-md-6">
                  <div className="form-group">
                    <label htmlFor="name">Name</label>
                    <input
                      className="form-control"
                      name="name"
                      type="text"
                      onChange={inputNameUpdated}
                    />
                  </div>
                </div>
                <div className="col-xl-3 col-md-6">
                  <div className="form-group">
                    <label htmlFor="massMin">Model</label>
                    <input
                      className="form-control"
                      name="massMin"
                      type="text"
                      onChange={inputModelUpdated}
                    />
                  </div>
                </div>
                <div className="col-md-3">
                  <div className="form-group">
                    <label htmlFor="massMax">Manufacturer</label>
                    <input
                      className="form-control"
                      name="massMax"
                      type="number"
                      onChange={inputManufacturerUpdated}
                    />
                  </div>
                </div>
                <div className="col-md-12">
                  <button
                    className="btn btn-primary btn-search"
                    onClick={handleSearch}
                  >
                    Rechercher
                  </button>
                  <button
                    className="btn btn-primary btn-success"
                    onClick={undoSearch}
                  >
                    Réinitialiser
                  </button>
                </div>
              </div>
              <table className="table">
                <thead>
                  <tr>
                    <th scope="col">#</th>
                    <th scope="col">Name</th>
                    <th scope="col">Manufacturer</th>
                    <th scope="col">Created on</th>
                    <th scope="col">Edited on</th>
                  </tr>
                </thead>
                {datas.map((data, i) => {
                  return (
                    <tbody key={i}>
                      <tr>
                        <th scope="row">
                          <Link
                            to={{
                              pathname: `/fiche-starship/${i + 1}`,
                              state: {
                                foo: { i }
                              }
                            }}
                          >
                            {i + 1}
                          </Link>
                        </th>
                        <td>{data.name}</td>
                        <td>{data.manufacturer}</td>
                        <td>{dateFormat(data.created, "dd/mm/yy")}</td>
                        <td>{dateFormat(data.editedm, "dd/mm/yy")}</td>
                      </tr>
                    </tbody>
                  );
                })}
              </table>
            </div>
          </main>
        </div>
      </div>
    </div>
  );
}

export default StarshipScreen;
