import React, { useState, useEffect } from "react";
import DatasDAO from "../../dao/DatasDAO";
import dateFormat from "dateformat";
import MenuLeft from "../MenuLeft";

function FichePeople(props) {
  const [datas, setDatas] = useState([]);

  useEffect(() => {
    const id = props.match.params.id;
    const urlFichePeopleDatas = "http://localhost:8081/people/" + id + "/";
    DatasDAO(urlFichePeopleDatas).then(res => {
      setDatas(res);
    });
  }, []);

  return (
    <div className="sb-nav-fixed">
      <nav className="sb-topnav navbar navbar-expand navbar-dark bg-dark">
        <a className="navbar-brand" href="index.html">
          SWAPI
        </a>
      </nav>
      <div id="layoutSidenav">
        <MenuLeft />
        <div id="layoutSidenav_content">
          <main>
            <div className="container-fluid">
              <h1 className="mt-4 text-center">Fiche {datas.name}</h1>
              <table className="table">
                <tbody>
                  <tr>
                    <th>Height</th>
                    <td>{datas.height}</td>
                  </tr>
                  <tr>
                    <th>Mass</th>
                    <td>{datas.mass}</td>
                  </tr>
                  <tr>
                    <th>Hair color</th>
                    <td>{datas.hair_color}</td>
                  </tr>
                  <tr>
                    <th>Skin color</th>
                    <td>{datas.skin_color}</td>
                  </tr>
                  <tr>
                    <th>Eye color</th>
                    <td>{datas.eye_color}</td>
                  </tr>
                  <tr>
                    <th>Birth year</th>
                    <td>{datas.birth_year}</td>
                  </tr>
                  <tr>
                    <th>Gender</th>
                    <td>{datas.gender}</td>
                  </tr>
                  <tr>
                    <th>Homeworld</th>
                    <td>{datas.homeworld}</td>
                  </tr>
                  <tr>
                    <th>Film</th>
                    <td>
                      <ol>
                        {datas.films &&
                          datas.films.map((film, i) => {
                            return <li key={i}>{film}</li>;
                          })}
                      </ol>
                    </td>
                  </tr>
                  <tr>
                    <th>Species</th>
                    <td>{datas.species}</td>
                  </tr>
                  <tr>
                    <th>Vehicles</th>
                    <td>
                      <ol>
                        {datas.vehicles &&
                          datas.vehicles.map((vehicle, i) => {
                            return <li key={i}>{vehicle}</li>;
                          })}
                      </ol>
                    </td>
                  </tr>
                  <tr>
                    <th>Starships</th>
                    <td>
                      <ol>
                        {datas.starships &&
                          datas.starships.map((starship, i) => {
                            return <li key={i}>{starship}</li>;
                          })}
                      </ol>
                    </td>
                  </tr>
                  <tr>
                    <th>Created</th>
                    <td>{dateFormat(datas.created, "dd/mm/yy")}</td>
                  </tr>
                  <tr>
                    <th>Edited</th>
                    <td>{dateFormat(datas.edited, "dd/mm/yy")}</td>
                  </tr>
                </tbody>
              </table>
            </div>
          </main>
        </div>
      </div>
    </div>
  );
}

export default FichePeople;
