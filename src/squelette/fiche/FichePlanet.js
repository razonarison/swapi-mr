import React, { useState, useEffect } from "react";
import DatasDAO from "../../dao/DatasDAO";
import dateFormat from "dateformat";
import MenuLeft from "../MenuLeft";

function FichePlanet(props) {
  const [datas, setDatas] = useState([]);

  useEffect(() => {
    const id = props.match.params.id;
    const urlFichePlanetDatas = "http://localhost:8081/planets/" + id + "/";
    console.log(urlFichePlanetDatas);
    DatasDAO(urlFichePlanetDatas).then(res => {
      setDatas(res);
    });
  }, []);

  return (
    <div className="sb-nav-fixed">
      <nav className="sb-topnav navbar navbar-expand navbar-dark bg-dark">
        <a className="navbar-brand" href="index.html">
          SWAPI
        </a>
      </nav>
      <div id="layoutSidenav">
        <MenuLeft />
        <div id="layoutSidenav_content">
          <main>
            <div className="container-fluid">
              <h1 className="mt-4 text-center">Fiche {datas.name}</h1>
              <table className="table">
                <tbody>
                  <tr>
                    <th>Rotation period</th>
                    <td>{datas.rotation_period}</td>
                  </tr>
                  <tr>
                    <th>Orbital period</th>
                    <td>{datas.orbital_period}</td>
                  </tr>
                  <tr>
                    <th>Diameter</th>
                    <td>{datas.diameter}</td>
                  </tr>
                  <tr>
                    <th>Climate</th>
                    <td>{datas.climate}</td>
                  </tr>
                  <tr>
                    <th>Gravity</th>
                    <td>{datas.gravity}</td>
                  </tr>
                  <tr>
                    <th>Terrain</th>
                    <td>{datas.terrain}</td>
                  </tr>
                  <tr>
                    <th>Surface water</th>
                    <td>{datas.surface_water}</td>
                  </tr>
                  <tr>
                    <th>Population</th>
                    <td>{datas.population}</td>
                  </tr>
                  <tr>
                    <th>Residents</th>
                    <td>{datas.residents}</td>
                  </tr>
                  <tr>
                    <th>Films</th>
                    <td>
                      <ol>
                        {datas.films &&
                          datas.films.map((film, i) => {
                            return <li key={i}>{film}</li>;
                          })}
                      </ol>
                    </td>
                  </tr>
                  <tr>
                    <th>Created</th>
                    <td>{dateFormat(datas.created, "dd/mm/yy")}</td>
                  </tr>
                  <tr>
                    <th>Edited</th>
                    <td>{dateFormat(datas.edited, "dd/mm/yy")}</td>
                  </tr>
                </tbody>
              </table>
            </div>
          </main>
        </div>
      </div>
    </div>
  );
}

export default FichePlanet;
