import React, { useState, useEffect } from "react";
import DatasDAO from "../../dao/DatasDAO";
import dateFormat from "dateformat";
import MenuLeft from "../MenuLeft";

function FicheStarship(props) {
  const [datas, setDatas] = useState([]);

  useEffect(() => {
    const id = props.match.params.id;
    const urlFicheStarshipsDatas =
      "http://localhost:8081/starships/" + id + "/";
    DatasDAO(urlFicheStarshipsDatas).then(res => {
      setDatas(res);
    });
  }, []);

  return (
    <div className="sb-nav-fixed">
      <nav className="sb-topnav navbar navbar-expand navbar-dark bg-dark">
        <a className="navbar-brand" href="index.html">
          SWAPI
        </a>
      </nav>
      <div id="layoutSidenav">
        <MenuLeft />
        <div id="layoutSidenav_content">
          <main>
            <div className="container-fluid">
              <h1 className="mt-4 text-center">Fiche {datas.name}</h1>
              <table className="table">
                <tbody>
                  <tr>
                    <th>Model</th>
                    <td>{datas.model}</td>
                  </tr>
                  <tr>
                    <th>Manufacturer</th>
                    <td>{datas.manufacturer}</td>
                  </tr>
                  <tr>
                    <th>Cost in credits</th>
                    <td>{datas.cost_in_credits}</td>
                  </tr>
                  <tr>
                    <th>Climate</th>
                    <td>{datas.climate}</td>
                  </tr>
                  <tr>
                    <th>Length</th>
                    <td>{datas.length}</td>
                  </tr>
                  <tr>
                    <th>Max atmosphering speed</th>
                    <td>{datas.max_atmosphering_speed}</td>
                  </tr>
                  <tr>
                    <th>Crew</th>
                    <td>{datas.crew}</td>
                  </tr>
                  <tr>
                    <th>Passenger</th>
                    <td>{datas.passengers}</td>
                  </tr>
                  <tr>
                    <th>Cargo capacity</th>
                    <td>{datas.cargo_capacity}</td>
                  </tr>
                  <tr>
                    <th>Hyperdrive rating</th>
                    <td>{datas.hyperdrive_rating}</td>
                  </tr>
                  <tr>
                    <th>MGLT</th>
                    <td>{datas.MGLT}</td>
                  </tr>
                  <tr>
                    <th>Starship class</th>
                    <td>{datas.consustarship_classmables}</td>
                  </tr>
                  <tr>
                    <th>Pilots</th>
                    <td>{datas.pilots}</td>
                  </tr>
                  <tr>
                    <th>Films</th>
                    <td>
                      <ol>
                        {datas.films &&
                          datas.films.map((film, i) => {
                            return <li key={i}>{film}</li>;
                          })}
                      </ol>
                    </td>
                  </tr>
                  <tr>
                    <th>Created</th>
                    <td>{dateFormat(datas.created, "dd/mm/yy")}</td>
                  </tr>
                  <th>
                    <th>Edited</th>
                    <td>{dateFormat(datas.edited, "dd/mm/yy")}</td>
                  </th>
                </tbody>
              </table>
            </div>
          </main>
        </div>
      </div>
    </div>
  );
}

export default FicheStarship;
